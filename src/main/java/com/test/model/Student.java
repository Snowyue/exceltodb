package com.test.model;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Setter
@Getter
@ToString
public class Student {

        private String id;
        private String name;
        private String subject;
        private String className;
        private double grade;
        //是否缺课
        private boolean ifCutClass;
        private String teacher;

}
