package com.test;

import com.test.model.Student;
import com.test.util.ExcelImportSheet;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.util.StringUtil;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class TestForParseExcel {
    public static void main(String[] args) throws ClassNotFoundException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchMethodException, IOException {
        Map<String , String> map = new HashMap<>();
        //表头与键值对的映射关系
        map.put("学号", "id");
        map.put("姓名" , "name");
        map.put("科目" , "subject");
        map.put("分数" , "grade");
        map.put("班级" , "className");
        map.put("任课教师" , "teacher");
        map.put("是否缺课" , "ifCutClass");
        try(
                //这里面的对象会自动关闭
                InputStream in = new FileInputStream(new File("C:\\Test\\Student.xlsx"));
                //用流来构建工作簿对象
                Workbook workbook = ExcelImportSheet.getTypeFromExtends(in , "Student.xlsx")
        ) {

            //根据名称获取单张表对象 也可以使用getSheetAt(int index)获取单张表的对象 获取第一张表
            Sheet sheet = workbook.getSheetAt(0);
            List<Student> list = ExcelImportSheet.getListFromExcel(sheet , Student.class , map);

            for (Student student : list) {
                //底层数据库操作 insert什么的
            if(student.getName() == null || "".equals(student.getName())
                    ||student.getSubject()== null || "".equals(student.getSubject()) ){
                System.out.println("222222222");
            }
                System.out.println(student.toString());
            }
        }catch(IOException exception) {
            exception.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            //写着好看的
        }
    }

}